import { BrowserModule } from '@angular/platform-browser';
import { NgModule }      from '@angular/core';

import { AppRoutingModule }        from './app-routing.module';
import { AppComponent }            from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'ag-grid-enterprise';
import { SearchModule }            from './features/search/search.module';
import { MatCardModule, MatToolbarModule } from '@angular/material';
import { HttpClientModule }        from '@angular/common/http';
import { FlexModule }              from '@angular/flex-layout';

import * as ag from 'ag-grid-enterprise';

const tot = ag;

@NgModule( {
  declarations : [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,

    // Features
    SearchModule,
    MatToolbarModule,
    FlexModule,
    MatCardModule
  ],
  providers : [],
  bootstrap : [ AppComponent ]
} )
export class AppModule {}
