import { Section } from './app.component';

export const folders: Section[ ] = [
  {
    name: 'Photos',
    updated: new Date('1/1/16'),
    creator: 'Super sayan'
  },
  {
    name: 'Recipes',
    updated: new Date('1/17/16'),
    creator: 'un mec cool'
  },
  {
    name: 'Work',
    updated: new Date('1/28/16'),
    creator: 'un marin qui boit'
  }
];

export const notes: Section[ ] = [
  {
    name: 'Vacation Itinerary',
    updated: new Date('2/20/16'),
    creator: 'le mec qui ecrit'
  },
  {
    name: 'Kitchen Remodel',
    updated: new Date('1/18/16'),
    creator: 'c\'est bien de le savoir)'
  }
];

export interface FakeData {
  title: string;
  icon: string;
  data: Section[];
}

export const fakeData = [
  {
    title: 'Excel',
    icon: 'excel',
    data: notes
  }, {
    title: 'Word',
    icon: 'word',
    data: folders
  }, {
    title: 'Ppt',
    icon: 'ppt',
    data: notes
  }, {
    title: 'Word',
    icon: 'word',
    data: folders
  }
];
