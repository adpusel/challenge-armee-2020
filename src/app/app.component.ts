import { Component, OnInit }  from '@angular/core';
import { IconServiceService } from './features/services/icon-service.service';

export interface Section {
  name: string;
  updated: Date;
  creator: string;
}

@Component( {
  selector : 'app-root',
  template : `
    <div [class.default-theme]="false" class="marine">
      <div>
        <div style="background: white" fxLayout fxLayoutAlign="center center">
          <img fxFlex="140px" src="./assets/Plan de travail 1.png">
        </div>
        <div>
          <app-search-container></app-search-container>
        </div>

      </div>
    </div>
  `,
  styles : [ `` ]

} )
export class AppComponent implements OnInit {

  constructor( private iconServiceService: IconServiceService ) {
  }

  ngOnInit(): void {
    this.iconServiceService.registerIcons();

    // this.themeClass = newThemeClass;

    // remove old theme class and add new theme class
    // we're removing any css class that contains '-theme' string but your theme classes can follow any pattern
    // const overlayContainerClasses = this.overlayContainer.getContainerElement().classList;
    // const themeClassesToRemove = Array.from(classList).filter((item: string) => item.includes('-theme'));
    // if (themeClassesToRemove.length) {
    //   overlayContainerClasses.remove(...themeClassesToRemove);
    // }
    // overlayContainerClasses.add(newThemeClass);
  }

}

//
// tslint:disable-next-line:max-line-length
// '[{"score":10.89079,"title":"20190304-0314_P26_EDG_Module méthode de conduite des opérations au niveau
// opératifs.pdf","author":"Unknown","encrypted":false,"filepath":"/Users/vincent/edg-data/Sauvegarde Sharepoint Juin 2019/Infos
// dfense/20190304-0314_P26_EDG_Module méthode de conduite des opérations au niveau opératifs.pdf","data":{"data.4":["Salles de groupe
// \n\nBriefing de décision : restitution CONOPS \n\nCPOIA \n\nSalles de groupe \n\nRappel sur le <em>PCIAT</em>"],"data.1":["qui explique
// l’organisation et le fonctionnement d’un poste de commandement \ninterarmées de théâtre (<em>PCIAT</em>"],"data.3":["La P26 sera
// organisée en 6 <em>PCIAT</em> (<em>PCIAT</em> 1: G1/G2 - <em>PCIAT</em> 2: G3/G4 - <em>PCIAT</em> 3: G5/G6 - J <em>PCIAT</em> \n4:
// G7/G8","- <em>PCIAT</em> 5: G9/G10 -  <em>PCIAT</em> 6: G11/G12).","L’armement des <em>PCIAT</em> est réalisé par les professeurs de
// groupes (PdG, fichier diffusé à partir du 07","Les PdG assureront l’accompagnement des travaux des <em>PCIAT</em> de leurs bi groupes
// respectifs lors des \nphases","Petits  matériels  de  bureautique,  affichage  <em>PCIAT</em>  1  à  6  à  l’entrée  des  salles  de
// groupe,"]}},{"score":4.850469,"title":"Programme des cours P26 V 01 10
// 2018.pdf","author":"Unknown","encrypted":false,"filepath":"/Users/vincent/edg-data/Sauvegarde Sharepoint Juin 2019/RefEns/Présentation enseignement/Programme des cours P26 V 01 10 2018.pdf","data":{"data.11":["Les EFFETS \n\nL'art opératif\n\nLe niveau opératif en opérations \n\nOrganisation et fonctionnement du <em>PCIAT</em>"],"data.12":["Pratique de la COPD en JOPG\n\nLa conduite des opérations au niveau opératif\n\n-\n\nTD\n\nFonctionnement d'un <em>PCIAT</em>","Découverte et appprentissage individuels en e-learning\n\n<em>PCIAT</em>\n\nConf. / TD La conduite au niveau opératif","Pratique du <em>PCIAT</em> par les ordres à produire\n\nIFESO (marché) + CPOIA \n\nExercice Coalition\n\nCOAL1\n\nConf"]}},{"score":4.5263186,"title":"20180827_NP_EdG_DA_Directive PédagogiqueP26_V8.docx","author":"VILLIAUMEY COL","encrypted":false,"filepath":"/Users/vincent/edg-data/Sauvegarde Sharepoint Juin 2019/RefEns/Organisation/20180827_NP_EdG_DA_Directive PédagogiqueP26_V8.docx","data":{"data.13":["le poste de chef J5 dans un <em>PCIAT</em> pour les aspects operationnels."],"data.46":["politico-militaire et relations avec la diplomatie, organisation du commandement, fonctionnement d'un <em>PCIAT</em>"]}},{"score":4.5263186,"title":"20180827_NP_EdG_DA_Directive PédagogiqueP26_V8.docx","author":"VILLIAUMEY COL","encrypted":false,"filepath":"/Users/vincent/edg-data/Sauvegarde Sharepoint Juin 2019/RefEns/Présentation enseignement/20180827_NP_EdG_DA_Directive PédagogiqueP26_V8.docx","data":{"data.13":["le poste de chef J5 dans un <em>PCIAT</em> pour les aspects operationnels."],"data.46":["politico-militaire et relations avec la diplomatie, organisation du commandement, fonctionnement d'un <em>PCIAT</em>"]}},{"score":4.2959356,"title":"20180827_NP_EdG_DA_Directive PédagogiqueP26_VDEF.pdf","author":"Unknown","encrypted":false,"filepath":"/Users/vincent/edg-data/Sauvegarde Sharepoint Juin 2019/RefEns/Organisation/20180827_NP_EdG_DA_Directive PédagogiqueP26_VDEF.pdf","data":{"data.4":["politico-militaire  et  relations  avec  la \ndiplomatie, organisation du commandement, fonctionnement d’un <em>PCIAT</em>"],"data.1":["équivalents) ; \nle poste de chef de bureau à l’EMA pour les aspects organiques ; \nle poste de chef J5 dans un <em>PCIAT</em>"]}},{"score":4.2959356,"title":"20180827_NP_EdG_DA_Directive PédagogiqueP26_VDEF.pdf","author":"Unknown","encrypted":false,"filepath":"/Users/vincent/edg-data/Sauvegarde Sharepoint Juin 2019/RefEns/Présentation enseignement/20180827_NP_EdG_DA_Directive PédagogiqueP26_VDEF.pdf","data":{"data.4":["politico-militaire  et  relations  avec  la \ndiplomatie, organisation du commandement, fonctionnement d’un <em>PCIAT</em>"],"data.1":["équivalents) ; \nle poste de chef de bureau à l’EMA pour les aspects organiques ; \nle poste de chef J5 dans un <em>PCIAT</em>"]}},{"score":2.7980368,"title":"Ins. ann. P26 V 01 10 2018.pdf","author":"Unknown","encrypted":false,"filepath":"/Users/vincent/edg-data/Sauvegarde Sharepoint Juin 2019/RefEns/Présentation enseignement/Ins. ann. P26 V 01 10 2018.pdf","data":{"data.8":["Il  utilise  un  e-\nlearning sur le fonctionnement d’un <em>PCIAT</em> (poste de commandement interarmées de théâtre"]}},{"score":0.97439986,"title":"Ins. ann. P26 V 01 10 2018.docx","author":"Choutet COL","encrypted":false,"filepath":"/Users/vincent/edg-data/Sauvegarde Sharepoint Juin 2019/RefEns/Présentation enseignement/Ins. ann. P26 V 01 10 2018.docx","data":{"data.138":["Il utilise un e-learning sur le fonctionnement d'un <em>PCIAT</em> (poste de commandement interarmees de theatre"]}}]'
